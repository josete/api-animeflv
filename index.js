var cloudscraper = require('cloudscraper');
const cheerio = require('cheerio');
const fastify = require('fastify')({ logger: true })
const request = require('request');
const NodeCache = require("node-cache");

const myCache = new NodeCache({ stdTTL: 18000 });

const ANIMEFLV = "https://animeflv.net"

var paginaCargada;

/*cloudscraper.get(ANIMEFLV, function (error, response, body) {
  if (error) {
    console.log('Error occurred');
  } else {
    paginaCargada = body;
    fastify.listen(3000, (err) => {
      if (err) {
        fastify.log.error(err)
        process.exit(1)
      }
      fastify.log.info(`server listening on ${fastify.server.address().port}`)
    });
  }
});*/
fastify.listen(3000, (err) => {
  if (err) {
    fastify.log.error(err)
    process.exit(1)
  }
  fastify.log.info(`server listening on ${fastify.server.address().port}`)
});

fastify.get('/recientes', (request, reply) => {
  value = myCache.get("episodios");
  if (value == undefined) {
    var $ = cheerio.load(paginaCargada);
    listaEpisodios = []
    numero = $('ul.ListEpisodios').find('li').toArray().length;
    $('ul.ListEpisodios').find('li').each(function (i, elem) {
      url = getUrlVideo(ANIMEFLV + $(this).find('a').attr("href")).then((enlace) => {
        e = {}
        e['enlace'] = enlace;
        e['imagen'] = ANIMEFLV + $(this).find('a').find('span.Image').find('img').attr('src');
        e['titulo'] = $(this).find('a').find('strong').text() + " -- " + $(this).find('a').find('span.Capi').text();
        listaEpisodios.push(e);
        numero--;
        if (numero == 0) {
          success = myCache.set("episodios", listaEpisodios);
          reply.send({ episodios: listaEpisodios });
        }
      });
    });
  } else {
    reply.send({ episodios: value });
  }
});

fastify.get('/buscar', (request, reply) => {
  value = myCache.get(request.query.nombre);
  if (value == undefined) {
    accederAPagina(ANIMEFLV + "/browse?q=" + request.query.nombre, true).then((body) => {
      var $ = cheerio.load(body);
      listaAnimes = []
      numero = $('ul.ListAnimes').find('li').toArray().length;
      $('ul.ListAnimes').find('li').each(function (i, elem) {
        a = {}
        a["titulo"] = $(this).find('article').find('a').find('h3').text();
        a["enlace"] = ANIMEFLV + $(this).find('article').find('a').attr('href');
        a["imagen"] = $(this).find('article').find('a').find("div.Image").find('figure').find('img').attr("src");
        listaAnimes.push(a);
        numero--;
        if (numero == 0) {
          success = myCache.set(request.query.nombre, listaAnimes);
          reply.send({ animes: listaAnimes });
        }
      });
    });
  } else {
    reply.send({ animes: value });
  }
});

fastify.get('/episodios', (request, reply) => {
  value = myCache.get(request.query.url);
  if (value == undefined) {
    accederAPagina(request.query.url, true).then((body) => {
      var $ = cheerio.load(body);
      var scripts = $('script').toString();
      listaEpisodios = [];
      var info = JSON.parse(scripts.split("var anime_info =")[1].split("var episodes =")[0].replace(";", ""));
      var episodios = JSON.parse(scripts.split("var episodes =")[1].split("var last_seen")[0].replace(";", ""));
      numero = episodios.length;
      episodios.forEach((episodio) => {
        e = {}
        e["nombre"] = info[1];
        e["enlaceAnime"] = request.query.url;
        e["enlace"] = ANIMEFLV + "/ver/" + episodio[1] + "/" + info[2] + "-" + episodio[0];
        e["imagen"] = "https://cdn.animeflv.net/screenshots/" + info[0] + "/" + episodio[0] + "/th_3.jpg";
        e["titulo"] = "Episodio " + episodio[0];
        listaEpisodios.push(e)
        numero--;
        if (numero == 0) {
          success = myCache.set(request.query.url, listaEpisodios);
          reply.send({ episodios: listaEpisodios });
        }
      });
    });
  } else {
    reply.send({ episodios: value });
  }
});

fastify.get('/play', (request, reply) => {
  value = myCache.get(request.query.enlace);
  if (value == undefined) {
    getUrlVideo(request.query.enlace).then((enlace) => {
      success = myCache.set(request.query.enlace, enlace);
      reply.send({ enlace: enlace });
    });
  } else {
    reply.send({ enlace: value });
  }
});

fastify.get('/explorar', (request, reply) => {
  value = myCache.get("explorar" + request.query.pagina);
  if (value == undefined) {
    accederAPagina(ANIMEFLV + "/browse?page=" + request.query.pagina, true).then((body) => {
      var $ = cheerio.load(body);
      listaAnimes = []
      numero = $('ul.ListAnimes').find('li').toArray().length;
      $('ul.ListAnimes').find('li').each(function (i, elem) {
        a = {}
        a["titulo"] = $(this).find('article').find('a').find('h3').text();
        a["enlace"] = ANIMEFLV + $(this).find('article').find('a').attr('href');
        a["imagen"] = $(this).find('article').find('a').find("div.Image").find('figure').find('img').attr("src");
        listaAnimes.push(a);
        numero--;
        if (numero == 0) {
          success = myCache.set("explorar" + request.query.pagina, listaAnimes);
          reply.send({ animes: listaAnimes });
        }
      });
    });
  } else {
    reply.send({ animes: value });
  }
});

fastify.get('/tope', (request, reply) => {
  value = myCache.get("tope");
  if (value == undefined) {
    accederAPagina(ANIMEFLV + "/browse", true).then((body) => {
      var $ = cheerio.load(body);
      botones = $("ul.pagination");
      penultimo1 = botones.find("li").toString();
      dividido = penultimo1.split("</li>");
      tope = dividido[dividido.length - 3].split("</a>")[0].split(">")[2];
      success = myCache.set("tope", tope);
      reply.send({ tope: tope });
    });
  } else {
    reply.send({ tope: value });
  }
});

var getUrlVideo = function (url) {
  return new Promise((resolve, reject) => {
    accederAPagina(url, true).then((body) => {
      var $ = cheerio.load(body);
      var scripts = $('script').toString();
      var enlaces = scripts.split("var videos =")[1].split("$(document)")[0].replace(";", "");
      var enlacesObjecto = JSON.parse(enlaces);
      var natsuki = enlacesObjecto["SUB"][0]["code"];
      var natsukiEnlace = natsuki.split("src=")[1].split("scrolling=")[0].replace("\\", "").replace('"', "").replace('"', "");
      accederAPagina(natsukiEnlace, true).then((body) => {
        $ = cheerio.load(body);
        scripts = $('script').toString();
        enlaceCheck = "https://s1.animeflv.net/" + scripts.split("var check_url =")[1].split(";")[0].replace("'", "").replace("'", "").replace('"', "").trim();
        accederAPagina(enlaceCheck, true).then((body) => {
          //console.log(JSON.parse(body).file);
          resolve(JSON.parse(body).file);
        });
      });
    });
  });
}

var accederAPagina = function (url, comprobar) {
  return new Promise((resolve, reject) => {
    if (comprobar) {
      cloudscraper.get(url, function (error, response, body) {
        if (error) {
          console.log('Error occurred');
        } else {
          resolve(body);
        }
      });
    } else {
      request(url, { json: true }, (err, res, body) => {
        if (err) { return console.log(err); }
        resolve(body);
      });
    }
  });
}